//
//  QuoteView.swift of project named tukiSwiftUI
//
//  Created by Aurore D (@Tuwleep) on 27/03/2023.
//
//  

import SwiftUI

var job = ["Cultivateur","Éducateur", "Pécheur", "Créateur", "Gestionnaire", "Voleur", "Chasseur", "Consommateur", "Revendeur"]
var object = ["de carottes", "de poissons", "d'abeilles", "de mensonges", "de fromages", "de cailloux", "d'ignorants", ]
var date = ["2019", "1993", "700 av JC", "hier"]

struct QuoteView: View {
    
    @State var quote: String = "Et toi, qui es-tu ?"
    
    var body: some View {
        ZStack{
            Color.background.ignoresSafeArea()
            VStack{
                Spacer()
                Text(quote)
                    .font(.largeTitle)
                    .foregroundColor(Color.primary)
                    .multilineTextAlignment(.center)
                Button(action: {
                    createRandomContent()
                }) {
                    Text("Je suis...")
                        .foregroundColor(Color.font)
                }
                .padding()
                .background(
                    RoundedRectangle(cornerRadius: 10)
                        .fill(Color.action)
                        .shadow(color: Color.border, radius: 2)
                    )
                Spacer()
            }
//            .fixed(width: 400, height: 60))
            .padding()
            .frame(width: 380)
            .background(
                RoundedRectangle(cornerRadius: 10)
                    .fill(Color.card)
                    .shadow(color: .border, radius: 2)
            )
            .padding()
        }
    }
    
    
}

extension QuoteView {
    func createRandomContent(){
        let randomIndexObject = Int.random(in: 0..<object.count)
        let randomIndexJob = Int.random(in: 0..<job.count)
        let randomIndexDate = Int.random(in: 0..<date.count)
        
        let randomObject = object[randomIndexObject]
        let randomJob = job[randomIndexJob]
        let randomDate = date[randomIndexDate]
        
        quote = "\(randomJob) \(randomObject) depuis \(randomDate)"
    }
}

struct QuoteView_Previews: PreviewProvider {
    static var previews: some View {
        QuoteView()
    }
}
