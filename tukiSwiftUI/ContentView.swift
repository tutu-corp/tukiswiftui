//
//  ContentView.swift of project named tukiSwiftUI
//
//  Created by Aurore D (@Tuwleep) on 27/03/2023.
//
//  

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            QuoteView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
