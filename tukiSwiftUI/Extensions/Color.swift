//
//  Colors.swift of project named tukiSwiftUI
//
//  Created by Aurore D (@Tuwleep) on 27/03/2023.
//
//  

import SwiftUI

extension Color {
    //Color : A9907E
    //Dark mode: 00005C
    //Use for: background
    static let background = Color("ColorBackground")
    
    
    //Color : 675D50
    //Dark mode: F0CAA3
    //Use for: font, onBackground, onCard, border
    static let onBackground = Color("ColorPrimary")
    static let onCard = Color("ColorPrimary")
    static let border = Color("ColorPrimary")
    static let onAction = Color("ColorPrimary")
    static let font = Color("ColorPrimary")
    
    //Color : ABC4AA
    //Dark mode: 3B185F
    //Use for: card
    static let card = Color("ColorCard")
    
    //Color : F3DEBA
    //Dark mode: C060A1
    //Use for: Button
    static let action = Color("ColorAction")
    
    
}
