//
//  tukiSwiftUIApp.swift of project named tukiSwiftUI
//
//  Created by Aurore D (@Tuwleep) on 27/03/2023.
//
//  

import SwiftUI

@main
struct tukiSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
