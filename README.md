# Tuki in SwfitUI
Tuki is an application to display a random title of work.

## General info
This project is based on an OpenClassroom tutorial. The basic training is done with UIKit. I redid it with swiftUI.

#### This project is created with :
* Swift 5
* IOS 16.2
* Xcode 14.0

## Screenshot
|Start|Random answer 1|Random answer 2|
|--|--|--|
|![Start](./captures/start.png)|![Random1](./captures/random1.png)|![Random2](./captures/random2.png)|